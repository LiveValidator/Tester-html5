# LiveValidator - Tester HTML5

[![build status](https://gitlab.com/LiveValidator/Tester-html5/badges/master/build.svg)](https://gitlab.com/LiveValidator/Tester-html5/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Tester-html5/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Tester-html5/commits/master)

This LiveValidator tester provides tests to polyfill HTML5 validations. An options parser is also included to detect the validations needed based on the HTML5 validation attributes on an input.

Find the project [home page and docs](https://chesedo.gitlab.io/LiveValidator/).
