/**
 * This adds the testers for the auto checks detector
 */

LiveValidator.translations[ 'en-us' ].minNumber = 'Should be more than or equal %d';
LiveValidator.Tester.prototype.min = function( value, min ) {
    if ( this.isNumber( value ) ) {
        if ( value < min ) {
            this.addError( this.getMessage( 'minNumber' ).replace( '%d', min ) );
            return false;
        } else {
            return true;
        }
    }

    return false;
};

LiveValidator.translations[ 'en-us' ].maxNumber = 'Should be less than or equal %d';
LiveValidator.Tester.prototype.max = function( value, max ) {
    if ( this.isNumber( value ) ) {
        if ( value > max ) {
            this.addError( this.getMessage( 'maxNumber' ).replace( '%d', max ) );
            return false;
        } else {
            return true;
        }
    }
    return false;
};

LiveValidator.translations[ 'en-us' ].minlength = 'Should be %d characters or more';
LiveValidator.Tester.prototype.minlength = function( value, min ) {
    if ( value.length < min ) {
        this.addError( this.getMessage( 'minlength' ).replace( '%d', min ) );
        return false;
    } else {
        return true;
    }
};

LiveValidator.translations[ 'en-us' ].maxlength = 'Should be %d characters or less';
LiveValidator.Tester.prototype.maxlength = function( value, max ) {
    if ( value.length > max ) {
        this.addError( this.getMessage( 'maxlength' ).replace( '%d', max ) );
        return false;
    } else {
        return true;
    }
};

LiveValidator.Tester.prototype.pattern = function( value, params ) {
    var regex;

    // Try to use the 'u' flag as the docs state
    // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
    try {
        regex = new RegExp( params.regex, 'u' );
    } catch ( e ) {
        regex = new RegExp( params.regex );
    }

    if ( !regex.test( value ) ) {
        this.addError( params.title );
        return false;
    } else {
        return true;
    }
};

LiveValidator.translations[ 'en-us' ].beNumber = 'Value should be a number';
LiveValidator.Tester.prototype.isNumber = function( value ) {
    if ( isNaN( Number( value ) ) ) {
        this.addError( this.getMessage( 'beNumber' ) );
        return false;
    } else {
        return true;
    }
};

LiveValidator.translations[ 'en-us' ].email = 'Looking for a valid email address';
LiveValidator.Tester.prototype.email = function( value, multiple ) {
    multiple = multiple || false; // Defaults to false

    // TODO: change to validation according to RFC2822 spec
    // From http://stackoverflow.com/questions/46155/validate-email-address-in-javascript

    var regex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;
    var emails = [];

    if ( multiple ) {
        emails = value.split( ',' );
    } else {
        emails.push( value );
    }

    for ( var i = 0; i < emails.length; i++ ) {
        if ( !regex.test( emails[ i ] ) ) {
            this.addError( this.getMessage( 'email' ) );
            return false;
        }
    }

    return true;
};
