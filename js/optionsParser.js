/**
 * Try to detect checks based on some input attributes ( to 'polyfill' for browsers not supporting them )
 * Based on https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
 */

// Get namespace ready
var LiveValidator = LiveValidator || {};

LiveValidator.html5Parser = function( element ) {

    // Scope-safe the object
    if ( !( this instanceof LiveValidator.html5Parser ) ) {
        return new LiveValidator.html5Parser( element );
    }

    this.element = element;
    this.checks = [];

    // TODO: date-time input also have `min` and `max` support which is still missing
    // Types of inputs to look for in each group
    this.types = {
        numerics: [ 'number', 'range' ],
        pattern: [ 'email', 'password', 'search', 'tel', 'text', 'url' ],
        textLength: [ 'textarea', 'email', 'password', 'search', 'tel', 'text', 'url' ],
        email: [ 'email' ]
    };
};

LiveValidator.html5Parser.prototype = {
    parse: function() {
        return { checks: this.getChecks() };
    },
    /**
     * Get the checks for this input
     *
     * @return {Array} Array of checks detected with their parameters
     */
    getChecks: function() {
        var type = this.element.type;

        if ( this.types.numerics.indexOf( type ) !== -1 ) {
            this._filterNumeric();
        }

        if ( this.types.textLength.indexOf( type ) !== -1 ) {
            this._filterTextLength();
        }

        if ( this.types.pattern.indexOf( type ) !== -1 ) {
            this._filterPattern();
        }

        if ( this.types.email.indexOf( type ) !== -1 ) {
            this._filterEmail();
        }

        // Return null if no checks are found for overwrites to work correctly
        if ( this.checks.length > 0 ) {
            return this.checks;
        } else {
            return null;
        }
    },
    /**
     * Check for `min` and `max` attributes on numeric inputs
     */
    _filterNumeric: function() {
        this._addCheck( 'min' );
        this._addCheck( 'max' );
    },
    /**
     * Check for `minlength`, `maxlength` attributes on form elements
     */
    _filterTextLength: function() {
        this._addCheck( 'minlength' );
        this._addCheck( 'maxlength' );
    },
    /**
     * Check for `pattern` attribute on "text" inputs
     */
    _filterPattern: function() {
        if ( this.element.hasAttribute( 'pattern' ) ) {
            var params = {};
            params.regex = this.element.getAttribute( 'pattern' );
            params.title = this.element.getAttribute( 'title' );
            this.checks.push( { 'pattern':  params } );
        }
    },
    /**
     * Check if email has multiple on or not to contruct its check
     */
    _filterEmail: function() {
        this.checks.push( { email: this.element.multiple } );
    },
    /**
     * Try to find the 'check' attribute and add its check if it exists
     *
     * @param  {string} check The attribte to look for. Eg. `min`
     */
    _addCheck: function( check ) {
        if ( this.element.hasAttribute( check ) ) {
            var checkObj = {};
            checkObj[ check ] = parseInt( this.element.getAttribute( check ) );
            this.checks.push( checkObj );
        }
    }
};

// Register itself as an optionsParser
LiveValidator.optionsParsers.push( LiveValidator.html5Parser );
