/**
 * The test suite for the html5 "tester" (js/testerHtml5.js)
 */

/* globals tester */
describe( 'HTML5 validation tester', function() {
    describe( 'min is', function() {
        tester.minSpec();
    } );
    describe( 'max is', function() {
        tester.maxSpec();
    } );
    describe( 'minlength is', function() {
        tester.minlengthSpec();
    } );
    describe( 'maxlength is', function() {
        tester.maxlengthSpec();
    } );
    describe( 'pattern', function() {
        tester.patternSpec();
    } );
    describe( 'isNumber', function() {
        tester.isNumberSpec();
    } );
    describe( 'email', function() {
        tester.emailSpec();
    } );
} );
