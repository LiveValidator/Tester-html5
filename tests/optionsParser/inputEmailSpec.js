var html5Parser = html5Parser || {};

html5Parser.inputEmailSpec = function() {
    beforeEach( function() {
        var instance = new LiveValidator.html5Parser( helper.createInput( 'email' ) );
        this.checks = instance.getChecks();
    } );

    it( 'not to have `min`', function() {
        expect( this.checks ).not.toContain( { min: 1 } );
    } );

    it( 'not to have `max`', function() {
        expect( this.checks ).not.toContain( { max: 10 } );
    } );

    it( 'to have `minlength`', function() {
        expect( this.checks ).toContain( { minlength: 1 } );
    } );

    it( 'to have `maxlength`', function() {
        expect( this.checks ).toContain( { maxlength: 2 } );
    } );

    it( 'to have `pattern`', function() {
        expect( this.checks ).toContain( { pattern: { regex: 'a-z', title: 'Helper title' } } );
    } );

    it( 'to have `email`', function() {
        expect( this.checks ).toContain( { email: true } );
    } );

    it( 'to have `email` with multiple set to true', function() {
        setFixtures( '<input type="email" multiple="true" />' );
        var input = document.getElementsByTagName( 'input' )[ 0 ];
        var instance = new LiveValidator.html5Parser( input );
        expect( instance.getChecks() ).toContain( { email: true } );
    } );

    it( 'to have `email` with multiple has to value', function() {
        setFixtures( '<input type="email" multiple />' );
        var input = document.getElementsByTagName( 'input' )[ 0 ];
        var instance = new LiveValidator.html5Parser( input );
        expect( instance.getChecks() ).toContain( { email: true } );
    } );

    it( 'to have `email` with multiple not set', function() {
        setFixtures( '<input type="email" />' );
        var input = document.getElementsByTagName( 'input' )[ 0 ];
        var instance = new LiveValidator.html5Parser( input );
        expect( instance.getChecks() ).toContain( { email: false } );
    } );
};
