var helper = helper || {};

helper.bareInput = function() {
    setFixtures( '<input />' );
    return document.getElementsByTagName( 'input' )[ 0 ];
};

helper.createInput = function( type ) {
    setFixtures( '<input type="' + type + '" min="1" max="10" minlength="1" maxlength="2" pattern="a-z" ' +
        ' title="Helper title" multiple/>' );
    return document.getElementsByTagName( 'input' )[ 0 ];
};
