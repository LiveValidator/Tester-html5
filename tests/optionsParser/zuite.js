/**
 * The test suite for the html5 options parser (js/optionsParser.js)
 */

/* globals html5Parser */
describe( 'Options Parser', function() {
    describe( 'check instantiation', function() {
        html5Parser.instantiationSpec();
    } );
    describe( 'check getChecks', function() {
        html5Parser.getChecksSpec();
    } );
    describe( 'check parse', function() {
        html5Parser.parseSpec();
    } );
    describe( 'check checkbox input', function() {
        html5Parser.inputCheckboxSpec();
    } );
    describe( 'check color input', function() {
        html5Parser.inputColorSpec();
    } );

    // TODO: Add `date`, `datetime` and `datetime-local` tests here
    describe( 'check email input', function() {
        html5Parser.inputEmailSpec();
    } );

    // TODO: Add `month` test here
    describe( 'check number input', function() {
        html5Parser.inputNumberSpec();
    } );
    describe( 'check password input', function() {
        html5Parser.inputPasswordSpec();
    } );
    describe( 'check range input', function() {
        html5Parser.inputRangeSpec();
    } );
    describe( 'check search input', function() {
        html5Parser.inputSearchSpec();
    } );
    describe( 'check tel input', function() {
        html5Parser.inputTelSpec();
    } );
    describe( 'check text input', function() {
        html5Parser.inputTextSpec();
    } );

    // TODO: Add `time` test here
    describe( 'check url input', function() {
        html5Parser.inputUrlSpec();
    } );

    // TODO: Add `week` test here
    describe( 'check textarea', function() {
        html5Parser.textareaSpec();
    } );

    it( 'auto registered itself as a parser', function() {
        expect( LiveValidator.optionsParsers.length ).toBe( 1 );
        expect( LiveValidator.optionsParsers ).toContain( LiveValidator.html5Parser );
    } );
} );
