var html5Parser = html5Parser || {};

html5Parser.getChecksSpec = function() {
    it( 'when valid check found', function() {
        var instance = new LiveValidator.html5Parser( helper.createInput( 'text' ) );

        expect( instance.getChecks() ).toContain( { minlength: 1 } );
    } );

    it( 'when no checks are found', function() {
        var instance = new LiveValidator.html5Parser( helper.bareInput() );

        expect( instance.getChecks() ).toBe( null );
    } );
};
