var html5Parser = html5Parser || {};

html5Parser.textareaSpec = function() {
    beforeEach( function() {
        setFixtures( '<textarea minlength="1" maxlength="2"></textarea>' );
        var input = document.getElementsByTagName( 'textarea' )[ 0 ];
        var instance = new LiveValidator.html5Parser( input );
        this.checks = instance.getChecks();
    } );

    it( 'not to have `min`', function() {
        expect( this.checks ).not.toContain( { min: 1 } );
    } );

    it( 'not to have `max`', function() {
        expect( this.checks ).not.toContain( { max: 10 } );
    } );

    it( 'to have `minlength`', function() {
        expect( this.checks ).toContain( { minlength: 1 } );
    } );

    it( 'to have `maxlength`', function() {
        expect( this.checks ).toContain( { maxlength: 2 } );
    } );

    it( 'not to have `pattern`', function() {
        expect( this.checks ).not.toContain( { pattern: { regex: 'a-z', title: 'Helper title' } } );
    } );

    it( 'not to have `email`', function() {
        expect( this.checks ).not.toContain( { email: true } );
    } );
};
