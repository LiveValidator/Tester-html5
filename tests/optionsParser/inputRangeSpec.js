var html5Parser = html5Parser || {};

html5Parser.inputRangeSpec = function() {
    beforeEach( function() {
        var instance = new LiveValidator.html5Parser( helper.createInput( 'range' ) );
        this.checks = instance.getChecks();
    } );

    it( 'to have `min`', function() {
        expect( this.checks ).toContain( { min: 1 } );
    } );

    it( 'to have `max`', function() {
        expect( this.checks ).toContain( { max: 10 } );
    } );

    it( 'not to have `minlength`', function() {
        expect( this.checks ).not.toContain( { minlength: 1 } );
    } );

    it( 'not to have `maxlength`', function() {
        expect( this.checks ).not.toContain( { maxlength: 2 } );
    } );

    it( 'not to have `pattern`', function() {
        expect( this.checks ).not.toContain( { pattern: { regex: 'a-z', title: 'Helper title' } } );
    } );

    it( 'not to have `email`', function() {
        expect( this.checks ).not.toContain( { email: true } );
    } );
};
