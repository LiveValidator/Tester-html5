var html5Parser = html5Parser || {};

html5Parser.parseSpec = function() {
    it( 'to return correct options', function() {
        var instance = new LiveValidator.html5Parser( helper.createInput( 'text' ) );

        expect( instance.parse() ).toEqual( {
            checks: [
                  { minlength: 1 },
                  { maxlength: 2 },
                  { pattern: { regex: 'a-z', title: 'Helper title' } }
            ]
        } );
    } );
};
