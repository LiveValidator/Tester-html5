var html5Parser = html5Parser || {};

html5Parser.instantiationSpec = function() {
    it( 'when called without `new`', function() {
        var tester = LiveValidator.html5Parser( helper.bareInput() );

        expect( tester.checks ).toBeDefined();
        expect( window.checks ).toBeUndefined();
    } );

    it( 'when called', function() {
        var tester = new LiveValidator.html5Parser( helper.bareInput() );

        expect( tester.checks ).toEqual( [] );
    } );
};
